
/*
 
 * Wrapfile.c (rewrite)

 * Programmer Name: Colin Mills

 * Date: December 17th 2014

 * Description: A rewrite of the wrapfile program  just to prove that I can do 
 * it.

*/

   /* Header File Includes */
   #include <stdio.h> // For the standard functions required.
   #include <string.h> // strcmp, strcpy (so far)

   /* Prototypes */
   int getFileLength(FILE* fileName);





  /* Constants */

  /* Specific Error Messages */
  char* FILE_NOT_OPENABLE = "The file was not openable\n";
  char* USAGE = "Usage: wrapfile[-w width] file...\n";

  /* Specific Return Values*/
  const int FILE_NOT_OPENABLE_VALUE = 1;
  const int INVALD_ARGS_VALUE = 2;
  const int OTHER_FATAL_ERROR_VALUE = 4;


  /* Logic */
  int main(int argc, char* argv[])
  {
  
      /* VARIABLE DEFINITION */

      /* Error Variables */
      char errorMessageBuffer[81] = ""; // Stores the appropriate error message string 
      int wrapfileReturnStatus = 0;    // Stores the approrpiate return value from main. 

      char* fileName = NULL;


      /* Loop Counters */
      int loopCounter = 0;

      /* The File Variables */
      FILE* textFileToWrap = NULL;
      int fileLength = 0;

      /* END VARIABLE DEFINITION */


      /* First lets check to see if argc is less then 2 */
      
      if (argc < 2)
      {
          strcpy(errorMessageBuffer, USAGE);
          printf("%s", errorMessageBuffer);
      }


      /* Else lets get the basic functionality going */
      else
      {
          textFileToWrap = fopen(argv[1], "r");

          if (textFileToWrap == NULL)
          {
              strcpy(errorMessageBuffer, FILE_NOT_OPENABLE);
              printf("%s", errorMessageBuffer);
              wrapfileReturnStatus = FILE_NOT_OPENABLE_VALUE;
          }

          else
          {
              fileLength = getFileLength(textFileToWrap);
              char fileContentsBuffer[fileLength] = "";


          }
          


          

      }

      return (wrapfileReturnStatus);

  }


/* 

   * Function Name: getFileLength

   * Parameters: 

   * FILE* fileName

   * Return Values:

   * An int representing the filename

   * Description:

   * Gets the filelength of a given file, note I am using the f(functions) 
   * because I am on a Linux system.

*/
  int getFileLength(FILE* fileName)
  {
      int fileLength = 0;

      fseek(fileName, 0, SEEK_END);
      fileLength = ftell(fileName);
      fseek(fileName, 0L, SEEK_SET);

      return (fileLength);
  }




